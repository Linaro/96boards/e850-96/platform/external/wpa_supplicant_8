/*
 * Vendor Hidl Interface for wpa_supplicant daemon
 *
 */

#include "hidl_manager.h"
#include "hidl_return_util.h"
#include "supplicantvendor.h"
#include "supplicant.h"

#include <android-base/file.h>
#include <fcntl.h>
#include <sys/stat.h>

namespace vendor {
namespace samsung_slsi {
namespace hardware {
namespace wifi {
namespace supplicantvendor{
namespace V1_0 {
namespace Implementation {
using android::hardware::wifi::supplicant::V1_2::implementation::hidl_return_util::validateAndCall;
using namespace android::hardware;
using namespace android::hardware::wifi::supplicant::V1_2;
using namespace android::hardware::wifi::supplicant::V1_2::implementation;
typedef android::hardware::wifi::supplicant::V1_2::ISupplicant ISupplicant;
using namespace vendor::samsung_slsi::hardware::wifi::supplicant::V1_0;

SupplicantVendor::SupplicantVendor(struct wpa_global* global) : wpa_global_(global) {}
bool SupplicantVendor::isValid()
{
	// This top level object cannot be invalidated.
	return true;
}

Return<void> SupplicantVendor::listVendorInterfaces(listVendorInterfaces_cb _hidl_cb)
{
	return validateAndCall(
	    this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
	    &SupplicantVendor::listVendorInterfacesInternal, _hidl_cb);
}

Return<void> SupplicantVendor::getVendorInterface(
    const ISupplicant::IfaceInfo& iface_info, getVendorInterface_cb _hidl_cb)
{
	return validateAndCall(
	    this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
	    &SupplicantVendor::getVendorInterfaceInternal, _hidl_cb, iface_info);
}

std::pair<SupplicantStatus, android::sp<ISupplicantVendorIface>>
SupplicantVendor::getVendorInterfaceInternal(const ISupplicant::IfaceInfo& iface_info)
{
	struct wpa_supplicant* wpa_s =
	    wpa_supplicant_get_iface(wpa_global_, iface_info.name.c_str());
	if (!wpa_s) {
		return {{SupplicantStatusCode::FAILURE_IFACE_UNKNOWN, ""},
			nullptr};
	}
	HidlManager* hidl_manager = HidlManager::getInstance();
	wpa_printf(MSG_INFO, "get vendor sta iface object");
	android::sp<ISupplicantVendorStaIface> vendor_iface;
	if (!hidl_manager ||
		hidl_manager->getVendorStaIfaceHidlObjectByIfname(
		wpa_s->ifname, &vendor_iface)) {
		return {{SupplicantStatusCode::FAILURE_UNKNOWN, ""},
				vendor_iface};
	}
	return {{SupplicantStatusCode::SUCCESS, ""}, vendor_iface};
}

std::pair<SupplicantStatus, std::vector<ISupplicant::IfaceInfo>>
SupplicantVendor::listVendorInterfacesInternal()
{
	std::vector<ISupplicant::IfaceInfo> ifaces;
	for (struct wpa_supplicant* wpa_s = wpa_global_->ifaces; wpa_s;
	     wpa_s = wpa_s->next) {
		if (wpa_s->global->p2p_init_wpa_s == wpa_s) {
			ifaces.emplace_back(ISupplicant::IfaceInfo{
			    IfaceType::P2P, wpa_s->ifname});
		} else {
			ifaces.emplace_back(ISupplicant::IfaceInfo{
			    IfaceType::STA, wpa_s->ifname});
		}
	}
	return {{SupplicantStatusCode::SUCCESS, ""}, std::move(ifaces)};
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace samsung_slsi
}  // namespace vendor
