/*
 * Vendor Hidl Station Interface for wpa_supplicant daemon
 *
 */

#ifndef WPA_SUPPLICANT_VENDOR_HIDL_STA_IFACE_H
#define WPA_SUPPLICANT_VENDOR_HIDL_STA_IFACE_H

#include <array>
#include <vector>

#include <android-base/macros.h>

#include <android/hardware/wifi/supplicant/1.0/ISupplicantStaIface.h>
#include <android/hardware/wifi/supplicant/1.0/ISupplicantStaIfaceCallback.h>
#include <android/hardware/wifi/supplicant/1.0/ISupplicantStaNetwork.h>
#include <vendor/samsung_slsi/hardware/wifi/supplicant/1.0/ISupplicantVendorStaIface.h>
#include <vendor/samsung_slsi/hardware/wifi/supplicant/1.0/ISupplicantVendorStaIfaceCallback.h>

extern "C" {
#include "utils/common.h"
#include "utils/includes.h"
#include "wpa_supplicant_i.h"
#include "config.h"
#include "driver_i.h"
#include "wpa.h"
#include "ctrl_iface.h"
}

namespace vendor {
namespace samsung_slsi {
namespace hardware {
namespace wifi {
namespace supplicantvendor {
namespace V1_0 {
namespace Implementation {
using namespace android::hardware::wifi::supplicant::V1_2;
using namespace android::hardware::wifi::supplicant::V1_2::implementation;
using namespace vendor::samsung_slsi::hardware::wifi::supplicant::V1_0;
using namespace android::hardware;

/**
 * Implementation of VendorStaIface hidl object. Each unique hidl
 * object is used for control operations on a specific interface
 * controlled by wpa_supplicant.
 */
class VendorStaIface : public ISupplicantVendorStaIface
{
public:
	VendorStaIface(struct wpa_global* wpa_global, const char ifname[]);
	~VendorStaIface() override = default;
	// HIDL does not provide a built-in mechanism to let the server
	// invalidate a HIDL interface object after creation. If any client
	// process holds onto a reference to the object in their context,
	// any method calls on that reference will continue to be directed to
	// the server.
	// However Supplicant HAL needs to control the lifetime of these
	// objects. So, add a public |invalidate| method to all |IVendorface| and
	// |VendorNetwork| objects.
	// This will be used to mark an object invalid when the corresponding
	// iface or network is removed.
	// All HIDL method implementations should check if the object is still
	// marked valid before processing them.
	void invalidate();
	bool isValid();
	Return<void> registerVendorCallback(
	    const android::sp<ISupplicantVendorStaIfaceCallback>& callback,
	    registerVendorCallback_cb _hidl_cb) override;

private:
	// Corresponding worker functions for the HIDL methods.
	SupplicantStatus registerCallbackInternal(
	    const android::sp<ISupplicantVendorStaIfaceCallback>& callback);
	struct wpa_supplicant* retrieveIfacePtr();
	// Reference to the global wpa_struct. This is assumed to be valid for
	// the lifetime of the process.
	struct wpa_global* wpa_global_;
	// Name of the iface this hidl object controls
	const std::string ifname_;
	bool is_valid_;
	// A macro to disallow the copy constructor and operator= functions
	// This must be placed in the private: declarations for a class.
	DISALLOW_COPY_AND_ASSIGN(VendorStaIface);
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace samsung_slsi
}  // namespace vendor
#endif  // WPA_SUPPLICANT_VENDOR_HIDL_STA_IFACE_H
