/*
 * Vendor Hidl Station Interface for wpa_supplicant daemon
 *
 */

#include "hidl_manager.h"
#include "hidl_return_util.h"
#include "iface_config_utils.h"
#include "misc_utils.h"
#include "sta_iface.h"
#include "vendorsta_iface.h"

extern "C" {
#include "utils/eloop.h"
#include "wps_supplicant.h"
}

namespace vendor {
namespace samsung_slsi {
namespace hardware {
namespace wifi {
namespace supplicantvendor {
namespace V1_0 {
namespace Implementation {
using android::hardware::wifi::supplicant::V1_2::implementation::hidl_return_util::validateAndCall;

VendorStaIface::VendorStaIface(struct wpa_global *wpa_global, const char ifname[])
    : wpa_global_(wpa_global), ifname_(ifname), is_valid_(true)
{
}

void VendorStaIface::invalidate() { is_valid_ = false; }
bool VendorStaIface::isValid()
{
	return (is_valid_ && (retrieveIfacePtr() != nullptr));
}

Return<void> VendorStaIface::registerVendorCallback(
    const android::sp<ISupplicantVendorStaIfaceCallback> &callback,
    registerVendorCallback_cb _hidl_cb)
{
	return validateAndCall(
	    this, SupplicantStatusCode::FAILURE_IFACE_INVALID,
	    &VendorStaIface::registerCallbackInternal, _hidl_cb, callback);
}
SupplicantStatus VendorStaIface::registerCallbackInternal(
    const android::sp<ISupplicantVendorStaIfaceCallback> &callback)
{
	HidlManager *hidl_manager = HidlManager::getInstance();
	if (!hidl_manager ||
	    hidl_manager->addVendorStaIfaceCallbackHidlObject(ifname_, callback)) {
		wpa_printf(MSG_INFO, "return failure vendor staiface callback");
		return {SupplicantStatusCode::FAILURE_UNKNOWN, ""};
	}
	return {SupplicantStatusCode::SUCCESS, ""};
}
/**
 * Retrieve the underlying |wpa_supplicant| struct
 * pointer for this iface.
 * If the underlying iface is removed, then all RPC method calls on this object
 * will return failure.
 */
wpa_supplicant *VendorStaIface::retrieveIfacePtr()
{
	return wpa_supplicant_get_iface(wpa_global_, ifname_.c_str());
}
}  // namespace implementation
}  // namespace V1_0
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace samsung_slsi
}  // namespace vendor
