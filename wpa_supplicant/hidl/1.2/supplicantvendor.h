/*
 * Vendor Hidl Interface for wpa_supplicant daemon
 *
 */

#ifndef WPA_SUPPLICANT_VENDOR_HIDL_SUPPLICANT_H
#define WPA_SUPPLICANT_VENDOR_HIDL_SUPPLICANT_H

#include <android-base/macros.h>

#include <android/hardware/wifi/supplicant/1.2/ISupplicant.h>
#include <android/hardware/wifi/supplicant/1.0/ISupplicantCallback.h>
#include <android/hardware/wifi/supplicant/1.0/ISupplicantIface.h>
#include <vendor/samsung_slsi/hardware/wifi/supplicant/1.0/ISupplicantVendor.h>
#include <vendor/samsung_slsi/hardware/wifi/supplicant/1.0/ISupplicantVendorIface.h>
#include <vendor/samsung_slsi/hardware/wifi/supplicant/1.0/ISupplicantVendorStaIface.h>

extern "C" {
#include "utils/common.h"
#include "utils/includes.h"
#include "utils/wpa_debug.h"
#include "wpa_supplicant_i.h"
}

namespace vendor {
namespace samsung_slsi {
namespace hardware {
namespace wifi {
namespace supplicantvendor{
namespace V1_0 {
namespace Implementation {
using namespace android::hardware::wifi::supplicant::V1_2;
using namespace android::hardware;
typedef android::hardware::wifi::supplicant::V1_2::ISupplicant ISupplicant;
using namespace vendor::samsung_slsi::hardware::wifi::supplicant::V1_0;
/**
 * Implementation of the supplicantvendor hidl object. This hidl
 * object is used core for global control operations on
 * wpa_supplicant.
 */
class SupplicantVendor : public vendor::samsung_slsi::hardware::wifi::supplicant::V1_0::ISupplicantVendor
{
public:
	SupplicantVendor(struct wpa_global* global);
	~SupplicantVendor() override = default;
	bool isValid();

	// Hidl methods exposed.
	Return<void> getVendorInterface(
	    const ISupplicant::IfaceInfo& iface_info, getVendorInterface_cb _hidl_cb) override;
	Return<void> listVendorInterfaces(listVendorInterfaces_cb _hidl_cb) override;
private:
	std::pair<SupplicantStatus, android::sp<ISupplicantVendorIface>> getVendorInterfaceInternal(
	    const ISupplicant::IfaceInfo& iface_info);
	std::pair<SupplicantStatus, std::vector<ISupplicant::IfaceInfo>>
	listVendorInterfacesInternal();
	// Raw pointer to the global structure maintained by the core.
	struct wpa_global* wpa_global_;
	// A macro to disallow the copy constructor and operator= functions
	// This must be placed in the private: declarations for a class.
	DISALLOW_COPY_AND_ASSIGN(SupplicantVendor);
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace supplicant
}  // namespace wifi
}  // namespace hardware
}  // namespace samsung_slsi
}  // namespace vendor

#endif  // WPA_SUPPLICANT_VENDOR_HIDL_SUPPLICANT_H
